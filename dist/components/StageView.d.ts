/// <reference types="react" />
import { StageProps } from './StageProps';
export declare const StageView: (stageProps: StageProps) => JSX.Element;
