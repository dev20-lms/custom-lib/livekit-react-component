/// <reference types="react" />
import { StageProps } from '../StageProps';
export declare const SpeakerStage: ({ roomState, participantRenderer, controlRenderer, onLeave, sortParticipants, }: StageProps) => JSX.Element;
