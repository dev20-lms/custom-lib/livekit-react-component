/// <reference types="react" />
import { StageProps } from '../StageProps';
export declare const GridStage: ({ roomState, participantRenderer, controlRenderer, onLeave, }: StageProps) => JSX.Element;
